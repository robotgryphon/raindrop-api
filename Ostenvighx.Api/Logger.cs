﻿using System;
using System.IO;
using System.Text;

namespace Ostenvighx.Api {

  /// <summary>
  /// A logger is a file used for writing information and output to a file.
  /// It is commonly used for debug information and related stuff.
  /// </summary>
  public class Logger {

    /// <summary>
    /// The file stream writer being used for writing.
    /// </summary>
    protected StreamWriter _logfile;

    /// <summary>
    /// The filename of the file being worked work.
    /// </summary>
    public String Filename {
      get;
      protected set;
    }

    /// <summary>
    /// Whether or not the file is currently open and able to be written to.
    /// </summary>
    public Boolean FileOpen {
      get;
      protected set;
    }

    /// <summary>
    /// Initialize a new instance of a logger.
    /// To begin logging, call StartLogging().
    /// </summary>
    /// <param name="filename">The filename to save the log file to.</param>
    public Logger(String filename) {
      this.Filename = filename;
      this.FileOpen = false;
    }

    /// <summary>
    /// Begin the process of logging to the file. You MUST call this
    /// before calling LogToFile.
    /// </summary>
    public void StartLogging() {
      this._logfile = new StreamWriter(Filename, true, Encoding.UTF8, 2048);

      this.FileOpen = true;

      _logfile.Write(_logfile.NewLine);
      LogToFile("Logging started.");
    }

    /// <summary>
    /// Write a string to the log file. This method automatically appends a timestamp
    /// to the beginning of the message. The timestamp looks like this:
    /// [12/31/2012 at 03:00:00 PM]
    /// </summary>
    /// <param name="message">The string to append to the file.</param>
    public void LogToFile(String message) {
      if (FileOpen && this._logfile != null) {
        DateTime Timestamp = DateTime.Now;
        String FormattedTimestamp = String.Format("[{0} at {1}] ", Timestamp.Date.ToShortDateString(), Timestamp.ToLongTimeString());
        _logfile.Write(FormattedTimestamp + message + _logfile.NewLine);
        _logfile.Flush();

      } else {
        throw new IOException("File not opened, cannot write to it.");
      }
    }


    /// <summary>
    /// Force write the changes and buffer to the file.
    /// </summary>
    public void Save() {
      _logfile.Flush();
    }

    /// <summary>
    /// Closes the file and stops logging to this file.
    /// To reactivate it, call StartLogging() before attempting to log.
    /// </summary>
    public void CloseFile() {
      LogToFile("Logging stopped.");

      this._logfile.Close();
      this._logfile.Dispose();
      this._logfile = null;

      this.FileOpen = false;
    }
  }
}
