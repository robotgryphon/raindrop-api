﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ostenvighx.Api {

  /// <summary>
  /// This class holds methods to simplify math conversions.
  /// </summary>
  public class MathHelper {

    /// <summary>
    /// Convert a given degree value into a radian value.
    /// </summary>
    /// <param name="degrees">An angle, specified in degrees.</param>
    /// <returns>An angle, converted into a radian value.</returns>
    public static double DegreesToRadians(float degrees) {
      return System.Math.PI * degrees / 180.0;
    }

    /// <summary>
    /// Convert a given radian value into a degree value.
    /// </summary>
    /// <param name="radians">An angle, specified in radians.</param>
    /// <returns>An angle, converted into a degree value.</returns>
    public static double RadiansToDegrees(float radians) {
      return radians * (180.0 / System.Math.PI);

    }
  }
}
