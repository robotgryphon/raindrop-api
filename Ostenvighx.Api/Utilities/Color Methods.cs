﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;

namespace Ostenvighx.Api.Utilities {

  /// <summary>
  /// This helper class is used for working with System.Drawing's colors.
  /// </summary>
  public class ColorHelper {

    /// <summary>
    /// Get a List object full of the pre-defined system colors.
    /// </summary>
    /// <returns>A List holding the System.Drawing colors.</returns>
    public static List<Color> GetSystemColorsList() {
      PropertyInfo[] propInfo = typeof(Color).GetProperties();

      List<Color> colors = new List<Color>();

      for (int propIndex = 0; propIndex < propInfo.Length; propIndex++) {

        if (propInfo[propIndex].PropertyType.IsAssignableFrom(typeof(Color)))
          colors.Add((Color) propInfo[propIndex].GetValue(typeof(Color), null));
      }

      return colors;
    }

    /// <summary>
    /// Given a list of colors and a randomizer, this method finds a random color.
    /// </summary>
    /// <param name="randomizer">A random object to use for color randomization.</param>
    /// <param name="colorList">A list of colors to pick from.</param>
    /// <returns>A random System.Drawing.Color from the list.</returns>
    /// <see cref="System.Drawing.Color" />
    public static Color GetRandomColor(Random randomizer, List<Color> colorList) {

      return colorList[randomizer.Next(0, colorList.Count)];
    }
  }
}
