﻿using System;
using System.Collections.Generic;

namespace Ostenvighx.Api.Utilities {

  /// <summary>
  /// This is a utility class designed for unit conversions.
  /// </summary>
  public class UnitConverter {

    /// <summary>
    /// Calculate a binary value from a string of ones and zeros.
    /// </summary>
    /// <param name="binaryString">String to convert.</param>
    /// <returns>A decimal with the numeric value of the binary string.</returns>
    public static double CalculateBinaryValue(String binaryString) {
      String currentString = binaryString;
      double binaryValue = 0;

      // Pop off first value and figure out calculated value by Math.Pow(2, position)
      while (currentString.Length > 0) {
        String currentDigit = currentString.Substring(0, 1);
        if (currentDigit == "0" || currentDigit == "1") {
          double maxValue = (currentDigit.Equals("1") ? 1 : 0) * System.Math.Pow(2, currentString.Length - 1);
          binaryValue += maxValue;
        } else {
          throw new FormatException("Found a malformed binary string. Error.");
        }

        currentString = currentString.Substring(1);
      }

      // Console.WriteLine("Value of {0}: {1}", binaryString, binaryValue);
      return binaryValue;
    }
  }
}
