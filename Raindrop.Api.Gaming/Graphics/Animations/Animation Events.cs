﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ostenvighx.Api.Gaming.Graphics.Animations {
  
  /// <summary>
  /// This class holds all the animation-related events.
  /// </summary>
  public class AnimationEvents {
    
    /// <summary>
    /// This delegate handles all the basic Animation events.
    /// </summary>
    /// <param name="sender">Sender of the event. (Usually an animation object)</param>
    /// <param name="args">Additional passed event arguments.</param>
    public delegate void AnimationEvent(Animation sender, EventArgs args);

  }
}
