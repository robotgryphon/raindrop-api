﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ostenvighx.Api.Gaming.Graphics;

namespace Ostenvighx.Api.Gaming.Graphics.Animations {
  public class AnimationException : Exception {

    public Animation thrower {
      get;
      protected set;
    }

    public AnimationException(Animation animation, String message) 
      :base(message){
        this.thrower = animation;
    }
  }
}
