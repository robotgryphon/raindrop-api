﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using Ostenvighx.Api.Gaming.Graphics;

namespace Ostenvighx.Api.Gaming.Graphics.Animations {

  /// <summary>
  /// An Animation is used for holding Keyframe data. It is used for smooth transitions with opacity,
  /// scaling, rotation, etc.
  /// </summary>
  public class Animation {

    /// <summary>
    /// A friendly name for the animation.
    /// </summary>
    protected String FriendlyName;

    /// <summary>
    /// A TextureSheet holding regions and texturing data for use with the Graphics side of the frames.
    /// </summary>
    protected TextureSheet Textures;

    /// <summary>
    /// Determines whether or not the animation should loop and restart when it finishes going through its.
    /// </summary>
    protected Boolean Looping;

    /// <summary>
    /// Specifies whether or not texturing data should be used during the keyframe.
    /// </summary>
    protected Boolean UseGraphics;

    /// <summary>
    /// Specifies whether or not alpha or opacity is being applied by the keyframe.
    /// </summary>
    protected Boolean UseAlpha;

    /// <summary>
    /// Specifies whether or not offset positioning is being applied by each keyframe.
    /// </summary>
    protected Boolean UsePositioning;

    /// <summary>
    /// A list of all the available frames in the animation.
    /// </summary>
    protected List<Keyframe> frames;

    /// <summary>
    /// Used for keeping track of how long the current frame has been displayed for.
    /// </summary>
    protected TimeSpan CurrentFrameDisplayTime;

    /// <summary>
    /// CurrentFrame is used to keep track of the index of the current frame
    /// in the animation.
    /// </summary>
    protected int CurrentFrame;

    /// <summary>
    /// Whether or not the animation has finished executing.
    /// </summary>
    public Boolean AnimationFinished {
      get;
      protected set;
    }

    /// <summary>
    /// Whether or not all the animation's frames shuold be reversed on the X-Axis.
    /// </summary>
    protected Boolean isReversedHorizontal;

    /// <summary>
    /// Whether or not all the animation's frames should be reversed on the Y-Axis.
    /// </summary>
    protected Boolean isReversedVertical;

    /// <summary>
    /// A friendly message to show in debug, showing the status of the animation.
    ///  (This is things like "Playing", "Paused", "Ready", etc.
    /// </summary>
    public String StatusMessage;

    /// <summary>
    /// This event is fired off when the animation finishes execution.
    /// </summary>
    public event AnimationEvents.AnimationEvent OnAnimationComplete;

    /// <summary>
    /// To create an animation, give it a name. You can fill in other information later.
    /// </summary>
    /// <param name="name"></param>
    public Animation(String name)
      : this(null, name) {
      this.UseGraphics = false;
    }

    /// <summary>
    /// To start using an animation, give it a name and a TExture Sheet to work with.
    /// </summary>
    /// <param name="sheet">The Texture Sheet object to work with. This object holds the texture and 
    /// display information for the frames to work with.</param>
    /// <param name="name">The name of this animation. Give it something unique, like "Jump_Begin", or "Stand_Forward".</param>
    public Animation(TextureSheet sheet, String name) {
      this.FriendlyName = name;
      this.Textures = sheet;

      this.Looping = false;
      this.UseAlpha = false;
      this.UseGraphics = true;
      this.UsePositioning = false;

      this.frames = new List<Keyframe>();
      Restart();

      isReversedHorizontal = false;
      isReversedVertical = false;

      this.StatusMessage = "Initialized and Ready.";
    }

    /// <summary>
    /// Restart the animation, setting the current frame to the first one and resetting any
    /// other dependant values to their initial states as well.
    /// </summary>
    public void Restart() {
      CurrentFrame = 0;
      CurrentFrameDisplayTime = TimeSpan.Zero;
      AnimationFinished = false;
      StatusMessage = "Animation ready.";
    }

    /// <summary>
    /// Returns the name of the animation.
    /// </summary>
    /// <returns>Name of animation.</returns>
    public String GetName() {
      return this.FriendlyName;
    }

    /// <summary>
    /// Enable the graphical side of the animation, by using a texture sheet.
    /// </summary>
    /// <param name="sheet">The texture sheet whose graphics you want to use for drawing.</param>
    public void EnableGraphics(TextureSheet sheet) {
      this.UseGraphics = true;
      this.Textures = sheet;
    }

    public void ToggleOpacity(Boolean status) {
      this.UseAlpha = status;
    }

    /// <summary>
    /// Allows for an entire animation to give SpriteEffects
    /// to its frames. Useful for when you already made a walking
    /// animation and you want to switch the direction.
    /// </summary>
    /// <param name="flipType"></param>
    public void ToggleFlipping(String flipType) {
      if (flipType == "none") {
        this.isReversedHorizontal = false;
        this.isReversedVertical = false;
      } else if (flipType == "horizontal") {
        this.isReversedHorizontal = true;
        this.isReversedVertical = false;
      } else if (flipType == "vertical") {
        this.isReversedHorizontal = false;
        this.isReversedVertical = true;
      }
    }

    /// <summary>
    /// Includes an additional frame to the end of the animation.
    /// </summary>
    /// <param name="frame">The frame to add to the animation.</param>
    public void AddFrame(Keyframe frame) {
      this.frames.Add(frame);
    }

    /// <summary>
    /// Returns the current frame that is being played in the animation.
    /// </summary>
    /// <returns>Frame object holding animation information.</returns>
    public Keyframe GetCurrentFrame() {
      return this.frames[CurrentFrame];
    }

    /// <summary>
    /// Get a frame in the animation.
    /// </summary>
    /// <param name="index">Index to search for in the animation.</param>
    /// <returns></returns>
    public Keyframe GetFrame(int index) {
      if (frames.Count == 0 || index > frames.Count || index < 0)
        return null;

      return this.frames[index];
    }

    /// <summary>
    /// Move to the next frame in the animation.
    /// </summary>
    /// <returns></returns>
    public Boolean NextFrame() {
      CurrentFrame++;
      if (CurrentFrame > frames.Count - 1) {
        if (Looping) {

          // Animation complete.
          CurrentFrame = 0;

          // Throw a "OnAnimationRestart" event here
        } else {
          // Throw a "OnAnimationFinished" event here
          AnimationFinished = true;
          StatusMessage = "Animation finished.";
        }

        if (this.OnAnimationComplete != null)
          this.OnAnimationComplete(this, EventArgs.Empty);

        return true;
      }

      return false;
    }

    /// <summary>
    /// Run an update on the animation.
    /// This allows the animation to move to its next frame, call events, etc.
    /// </summary>
    /// <param name="elapsedTime">Game time system.</param>
    public void Update(TimeSpan elapsedTime) {

      if (!AnimationFinished) {

        this.CurrentFrameDisplayTime += elapsedTime;

        this.StatusMessage = "Playing frame " + CurrentFrame + ", " + (GetCurrentFrame().Duration - CurrentFrameDisplayTime) + " remains";

        if (CurrentFrameDisplayTime >= GetCurrentFrame().Duration) {
          CurrentFrameDisplayTime = TimeSpan.Zero;
          NextFrame();
        }
      }
    }

    /// <summary>
    /// Using the texture data from the Texture Sheet and the Keyframe the
    /// Animation is currently on, draw the texturing information to the screen.
    /// </summary>
    /// <param name="elapsedTime"></param>
    public virtual void DrawTexture(TimeSpan elapsedTime) {
      
      /*
       * We actually can't do anything here.
       * Because each platform is different, we can't anticipate
       * how they will handle their drawing calls.
       * 
       * We just have to hope for the best, and tell the person coding
       * the platform animation system that they should have a method
       * that looks something like this.
       */

    }

    /// <summary>
    /// Display a helpful string printout of the animation's friendly name and number
    /// of keyframes.
    /// </summary>
    /// <returns>String with the value "{AnimationName}: {# of KeyFrames}"</returns>
    public override string ToString() {
      return this.FriendlyName + ": " + this.frames.Count.ToString();
    }

  }
}
