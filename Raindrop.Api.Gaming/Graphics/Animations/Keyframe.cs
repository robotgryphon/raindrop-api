﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Ostenvighx.Api.Gaming.Graphics.Animations {
  /// <summary>
  /// A Keyframe manages a single frame in an Animation.
  /// It holds information on the opacity, position, scale, and texture data.
  /// </summary>
  public class Keyframe {
    /// <summary>
    /// How long this frame should last before moving on to the next one.
    /// </summary>
    public TimeSpan Duration;

    /// <summary>
    /// Texture information to be displayed.
    /// </summary>
    public TextureRegion Texture;

    /// <summary>
    /// Opacity rendering of the frame.
    /// </summary>
    public byte Opacity;

    /// <summary>
    /// How far off a given "draw" location should the frame be drawn.
    /// </summary>
    public PointF Offset;

    /// <summary>
    /// Frame scaling.
    /// </summary>
    public float Scale;

    public Keyframe(TimeSpan duration)
      : this(duration, null, 100, PointF.Empty, 1) {
    }

    public Keyframe(TimeSpan duration, TextureRegion texture)
      : this(duration, texture, 100, PointF.Empty, 1) {
    }

    public Keyframe(TimeSpan duration, TextureRegion texture, byte opacity, PointF offset, float scale) {
      this.Duration = duration;
      this.Texture = texture;
      this.Opacity = opacity;
      this.Offset = offset;
      this.Scale = scale;
    }
  }
}
