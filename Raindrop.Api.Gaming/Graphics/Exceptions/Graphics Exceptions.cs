
using System;

namespace Ostenvighx.Api.Gaming {

	public class GraphicsException : Exception {

		public GraphicsException(String message)
			:base(message){

		}
	}
}

