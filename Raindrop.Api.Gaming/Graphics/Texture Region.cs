using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

using Ostenvighx.Api;
using Ostenvighx.Api.Utilities;

namespace Ostenvighx.Api.Gaming.Graphics {

  /// <summary>
  /// A TextureRegion is a box holding details about a section of a given TextureSheet.
  /// </summary>
  public class TextureRegion {

    protected TextureSheet ParentSheet;

    // Shared Dimensional Values
    protected Rectangle RegionalArea;

    /// <summary>
    /// Specifies how far off a location this region should be drawn.
    /// </summary>
    public PointF Origin {
      get;
      protected set;
    }

    // 3D Texture Values
    public UVPosition UV_Positioning;

    /// <summary>
    /// To create a Texture Region, give it the area to display on the spritesheet.
    /// </summary>
    /// <param name="area">The coordinates and positioning of the region on the texture sheet.</param>
    public TextureRegion(TextureSheet sheet, Rectangle area)
      : this(sheet, area, PointF.Empty) {
    }

    /// <summary>
    /// To give a region more advanced use, give it some more values to work with. 
    /// This gives you complete control over how a region is displayed.
    /// </summary>
    /// <param name="sheet">The texture sheet this region is on.</param>
    /// <param name="Area">The area and positioning of the region on the spritesheet.</param>
    /// <param name="Rotation">Rotation in degrees of the region.</param>
    /// <param name="Origin">Position from which drawing and math should be based off of.</param>
    public TextureRegion(TextureSheet sheet, Rectangle Area, PointF Origin) {
      this.ParentSheet = sheet;
      this.RegionalArea = Area;
      this.Origin = Origin;
    }

    public Rectangle GetArea() {
      return this.RegionalArea;
    }

    public UVPosition GetUVPosition() {
      return this.UV_Positioning;
    }
  }
}
