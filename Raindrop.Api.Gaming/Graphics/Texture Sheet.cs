﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ostenvighx.Api.Gaming.Graphics {

  /// <summary>
  /// This class models how a framework should manage a set of textures.
  /// </summary>
  public abstract class TextureSheet {

    /// <summary>
    /// A dictionary mapping of all the regions defined in this texture sheet.
    /// </summary>
    protected Dictionary<String, TextureRegion> Regions;

    /// <summary>
    /// Returns whether or not a region with the specified name is defined in this sheet.
    /// </summary>
    /// <param name="regionName">The name of the region to search for.</param>
    /// <returns>Whether or not the region exists in the sheet.</returns>
    public Boolean HasRegionDefined(String regionName) {
      return Regions.ContainsKey(regionName);
    }

    /// <summary>
    /// Return a reference to a region, attempting to get it by its defined name.
    /// </summary>
    /// <param name="regionName">The name the region is using.</param>
    /// <returns>A reference to the named texture region.</returns>
    public TextureRegion GetRegion(String regionName) {
      if (this.Regions.Keys.Contains(regionName)) {
        return this.Regions[regionName];
      }

      return null;
    }

    /// <summary>
    /// Add a named texture region to the sheet.
    /// </summary>
    /// <param name="regionName">The name to call the region.</param>
    /// <param name="region">The texture region itself.</param>
    public void AddRegion(String regionName, TextureRegion region) {
      if (this.Regions.Keys.Contains(regionName)) {
        throw new GraphicsException("Region with name '" + regionName + "' already exists in this sheet. Please change the name or modify the existing region instead.");
      } else {
        this.Regions.Add(regionName, region);
      }
    }

    /// <summary>
    /// Performs a force-overwrite of a named region, replacing it with another.
    /// </summary>
    /// <param name="regionName">The name of the region to replace.</param>
    /// <param name="region">The new region information.</param>
    public void ModifyRegion(String regionName, TextureRegion region) {
      this.Regions[regionName] = region;
    }

    /// <summary>
    /// Get the texture file the platform is using.
    /// </summary>
    /// <returns>A texture file pointer of some sort.</returns>
    public abstract Object GetTexture();

  }
}
