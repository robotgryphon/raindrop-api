﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ostenvighx.Api.Gaming.Graphics;
using System.Drawing;

namespace Ostenvighx.Api.Gaming.Graphics.UI {

  /// <summary>
  /// The Screen object is a basic screen that holds information to display 2D graphical information
  /// in a sort of "Window" on the screen.
  /// </summary>
  public abstract class UserInterface {

    public UserInterfaceManager InterfaceManager {
      get;
      protected set;
    }

    public String FriendlyName {
      get;
      protected set;
    }

    public Guid Identifier {
      get;
      protected set;
    }

    public Boolean RequiresInput;
    public Boolean AllowGameUpdates;
    public Boolean AllowPlayerMovement;
    public Boolean AllowClose;
    public Boolean DisplayPrevious;

    public Rectangle Boundaries {
      get;
      protected set;
    }

    public TextureSheet Textures {
      get;
      protected set;
    }

    // public event Screen_Events.ScreenEvent OnScreenOpened;
    // public event Screen_Events.ScreenEvent OnScreenClosed;

    // Manage a screen full of objects
    public UserInterface(UserInterfaceManager manager, String name) {
      this.InterfaceManager = manager;
      this.FriendlyName = name;
      this.Identifier = Guid.NewGuid();

      this.RequiresInput = false;
      this.AllowGameUpdates = false;
      this.AllowPlayerMovement = false;
      this.AllowClose = true;
      this.DisplayPrevious = false;

      this.Boundaries = Rectangle.Empty;

      manager.AddInterface(this);
    }

    public virtual void SetBoundaries(Rectangle bounds) {
      this.Boundaries = bounds;
    }

    public void SetTextures(TextureSheet tex) {
      this.Textures = tex;
    }

    public abstract void Update();

    public abstract void Draw();

  }
}
