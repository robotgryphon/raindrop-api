﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Ostenvighx.Api.Gaming.Graphics
{
    public struct UVPosition
    {

        public PointF Top_Left;
        public PointF Top_Right;
        public PointF Bottom_Left;
        public PointF Bottom_Right;

        public UVPosition(PointF tl, PointF tr, PointF bl, PointF br)
        {
            Top_Left = tl;
            Top_Right = tr;
            Bottom_Left = bl;
            Bottom_Right = br;
        }
    }
}
