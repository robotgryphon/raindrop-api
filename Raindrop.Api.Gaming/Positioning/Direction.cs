﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Ostenvighx.Api.Gaming.Positioning {

  /// <summary>
  /// A Direction specifies relative locations and movement methods.
  /// </summary>
  public class Direction {

    /// <summary>
    /// A Relative Direction is used for determining.. you guessed it. Relative directions.
    /// </summary>
    public enum RelativeDirection {

      /// <summary>
      /// The direction is outside of a boundary, or in a similar condition.
      /// </summary>
      Outside = 2,

      /// <summary>
      /// The direction is above a boundary or location. On an X/Y/Z coordinate system,
      /// Up is defined as being in the positive Y direction.
      /// </summary>
      Up = 3,

      /// <summary>
      /// The direction is below a boundary or location.
      /// </summary>
      Down = 4,

      /// <summary>
      /// The direction is north of a boundary or location. On an X/Y/Z coordinate system,
      /// North is defined as being in the positive X direction.
      /// </summary>
      North = 6,

      /// <summary>
      /// This direction is northwest of a boundary or location.
      /// </summary>
      NorthWest = 7,

      /// <summary>
      /// This direction is northeast of a boundary or location.
      /// </summary>
      NorthEast = 8,

      /// <summary>
      /// This direction is south of a boundary or location. On an X/Y/Z coordinate system,
      /// South is defined as being in the negative X direction.
      /// </summary>
      South = 9,

      /// <summary>
      /// This direction is southwest of a boundary or location.
      /// </summary>
      SouthWest = 10,

      /// <summary>
      /// This direction is southeast of a boundary or location.
      /// </summary>
      SouthEast = 11,

      /// <summary>
      /// The direction is inside a boundary, or in a similar condition.
      /// </summary>
      Inside = 12,

      /// <summary>
      /// This direction is west of a boundary or location. On an X/Y/Z coordinate system,
      /// West is defined as being in the negative Z direction.
      /// </summary>
      West = 13,

      /// <summary>
      /// This direction is east of a boundary or location. On an X/Y/Z coordinate system,
      /// East is defined as being in the positive Z direction.
      /// </summary>
      East = 14,

      /// <summary>
      /// This direction is mostly used for relative movements. A good example of using this is when
      /// a camera is looking in a direction and needs to move relative to that direction.
      /// </summary>
      Forward = 15,

      /// <summary>
      /// This direction is mostly used for relative movements. A good example of using this is when
      /// a camera is looking in a direction and needs to move relative to that direction.
      /// </summary>
      Backward = 16,

      /// <summary>
      /// This direction is mostly used for relative movements. A good example of using this is when
      /// a camera is looking in a direction and needs to move relative to that direction.
      /// </summary>
      Left = 17,

      /// <summary>
      /// This direction is mostly used for relative movements. A good example of using this is when
      /// a camera is looking in a direction and needs to move relative to that direction.
      /// </summary>
      Right = 18,

      /// <summary>
      /// An unknown direction.
      /// </summary>
      Unknown = 100
    };

    #region Fields

    /// <summary>
    /// Specifies how movement should occur.
    /// </summary>
    public Point3D MovementAmount {
      get;
      protected set;
    }

    #endregion

    /// <summary>
    /// Create a new direction, defaulting to move North.
    /// </summary>
    public Direction()
      : this(GetDirectionFromRelative(RelativeDirection.North)) {

    }

    /// <summary>
    /// Create a new direction, moving in a relative direction to start.
    /// </summary>
    /// <param name="relative">The relative direction to move in.</param>
    public Direction(RelativeDirection relative)
      : this(GetDirectionFromRelative(relative)) {
    }

    /// <summary>
    /// Create a new direction, moving in a specified amount to start.
    /// </summary>
    /// <param name="dir">The amount to move in as a Point3D.</param>
    public Direction(Point3D dir) {
      this.MovementAmount = dir;
    }

    /// <summary>
    /// Create a new direction from an existing one.
    /// </summary>
    /// <param name="copier">The direction to copy.</param>
    public Direction(Direction copier) {
      this.MovementAmount = copier.Duplicate().MovementAmount;
    }

    /// <summary>
    /// Set the movement amount to a new value.
    /// </summary>
    /// <param name="amount">The new movement amount value.</param>
    public void SetMovement(Point3D amount) {
      this.MovementAmount = amount;
    }

    #region Static Methods
    /// <summary>
    /// Is the coordinate given inside the bounds given?
    /// </summary>
    /// <param name="bounds">A rectangle to define for boundaries.</param>
    /// <param name="point">A coordinate to check for.</param>
    /// <returns>Whether or not the point given is within the bounds given.</returns>
    public static Boolean IsCoordinateInsideRectangle(Rectangle bounds, Point3D point) {
      return ((point.X >= bounds.X && point.X <= bounds.X + bounds.Width) && (point.Y >= bounds.Y && point.Y <= bounds.Y + bounds.Height));
    }

    /// <summary>
    /// Figure out which direction a point is in in relation to a boundary area.
    /// </summary>
    /// <param name="bounds">The boundaries to check.</param>
    /// <param name="point">The point to get a relation from.</param>
    /// <returns>A Direction, to define where the point is in relation to the boundaries.</returns>
    public static RelativeDirection RelationToBounds(Rectangle bounds, Point3D point) {

      if (IsCoordinateInsideRectangle(bounds, point))
        return RelativeDirection.Inside;

      int FinalPosition = 6;
      if (point.Y < bounds.Top) {
        // Point is north, northwest, or northeast of rectangle
      } else if (point.Y > bounds.Bottom) {
        // Point is south, southwest, or southeast of rectangle
        FinalPosition += 3;
      } else {
        // We are on level with the box
        FinalPosition += 6;
      }

      if (point.X < bounds.X) {
        FinalPosition += 1;
      } else if (point.X > bounds.Right) {
        FinalPosition += 2;
      }

      return (RelativeDirection) FinalPosition;
    }

    /// <summary>
    /// Not yet fully implemented. Will eventually check which direction pointToCheck is in
    /// in relation to the reference point.
    /// </summary>
    /// <param name="reference">The first point of reference.</param>
    /// <param name="pointToCheck">The point to check.</param>
    /// <returns></returns>
    public static RelativeDirection GetRelationalDirection(Point3D reference, Point3D pointToCheck) {
      if (IsCoordinateInsideRectangle(new Rectangle((int) reference.X, (int) reference.Y, 1, 1), pointToCheck))
        return RelativeDirection.Inside;

      // Direction is outside
      return RelativeDirection.Outside;
    }

    /// <summary>
    /// Return the opposite direction of a point.
    /// </summary>
    /// <param name="dir">Direction to get opposite of.</param>
    /// <returns></returns>
    public static RelativeDirection GetOppositeDirection(RelativeDirection dir) {
      switch (dir) {
        case RelativeDirection.Inside:
          return RelativeDirection.Outside;

        case RelativeDirection.North:
          return RelativeDirection.South;

        case RelativeDirection.South:
          return RelativeDirection.North;

        case RelativeDirection.West:
          return RelativeDirection.East;

        case RelativeDirection.East:
          return RelativeDirection.West;

        case RelativeDirection.NorthWest:
          return RelativeDirection.SouthEast;

        case RelativeDirection.NorthEast:
          return RelativeDirection.SouthWest;

        case RelativeDirection.SouthWest:
          return RelativeDirection.NorthEast;

        case RelativeDirection.SouthEast:
          return RelativeDirection.NorthWest;

        case RelativeDirection.Up:
          return RelativeDirection.Down;

        case RelativeDirection.Down:
          return RelativeDirection.Up;

        default:
          return RelativeDirection.Unknown;
      }
    }

    /// <summary>
    /// Get a point3D holding information on how to move using a Relative Direction.
    /// </summary>
    /// <param name="dir">The direction to move in.</param>
    /// <returns>A direction specifying how far to move.</returns>
    public static Direction GetDirectionFromRelative(Direction.RelativeDirection dir) {
      Point3D dir2 = new Point3D();

      switch (dir) {
        case Direction.RelativeDirection.North:
          // Move forward
          dir2.X = 1f;
          break;

        case Direction.RelativeDirection.South:
          dir2.X = -1;
          break;

        case Direction.RelativeDirection.West:
          dir2.Z = -1;
          break;

        case Direction.RelativeDirection.East:
          dir2.Z = 1;
          break;

        case Direction.RelativeDirection.Up:
          dir2.Y = 1;
          break;

        case Direction.RelativeDirection.Down:
          dir2.Y = -1;
          break;

        default:
          break;
      }

      return new Direction(dir2);
    }
    #endregion

    public Direction Duplicate() {
      Direction newD = new Direction(this.MovementAmount);
      return newD;
    }
  }
}
