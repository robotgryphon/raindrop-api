﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Ostenvighx.Api.Gaming.Positioning {

  /// <summary>
  /// A Point3D manages a 3D coordinate in an X-Y-Z space.
  /// </summary>
  public partial class Point3D {

    /// <summary>
    /// Get the origin point of (0, 0, 0).
    /// </summary>
    public static Point3D Origin = new Point3D(0, 0, 0);

    /// <summary>
    /// A single unit on the X-Axis.
    /// </summary>
    public static Point3D UnitX = new Point3D(1, 0, 0);

    /// <summary>
    /// A single unit on the Y-Axis.
    /// </summary>
    public static Point3D UnitY = new Point3D(0, 1, 0);

    /// <summary>
    /// A single unit on the Z-Axis.
    /// </summary>
    public static Point3D UnitZ = new Point3D(0, 0, 1);

    /// <summary>
    /// The X-Coordinate of this point.
    /// </summary>
    public float X;

    /// <summary>
    /// The Y-Coordinate of this point.
    /// </summary>
    public float Y;

    /// <summary>
    /// The Z-Coordinate of this point.
    /// </summary>
    public float Z;

    /// <summary>
    /// Create a default point in 3D space with the origin coordinates. (0, 0, 0)
    /// </summary>
    public Point3D() : this(0, 0, 0) {
    }

    /// <summary>
    /// Create a point in 3D space with a specified X- and Y-coordinate defined.
    /// This is useful for 2D math, since the Z-coordinate is set at zero.
    /// </summary>
    /// <param name="x">The position of the point on the X-axis.</param>
    /// <param name="y">The position of the point on the Y-axis.</param>
    public Point3D(float x, float y) : this(x, y, 0) {
    }

    /// <summary>
    /// Create a point in 3D space with a specified X-, Y-, and Z-coordinate defined.
    /// </summary>
    /// <param name="x">The position of the point on the X-axis.</param>
    /// <param name="y">The position of the point on the Y-axis.</param>
    /// <param name="z">The position of the point on the Z-axis.</param>
    public Point3D(float x, float y, float z) {
      this.X = x;
      this.Y = y;
      this.Z = z;
    }

    /// <summary>
    /// Compares two points to see if their X, Y, and Z coordinates are equal.
    /// </summary>
    /// <param name="p1">The point on the left to compare.</param>
    /// <param name="p2">The other point to compare to the first point.</param>
    /// <returns>Boolean specifying whether or not the two points have equal X, Y, and Z coordinates.</returns>
    public static bool operator ==(Point3D p1, Point3D p2) {
      return p1.Equals(p2);
    }

    /// <summary>
    /// Compares two points to see if their X, Y, and Z coordinates are not equal.
    /// </summary>
    /// <param name="p1">The point on the left to compare.</param>
    /// <param name="p2">The other point to compare to the first point.</param>
    /// <returns>Boolean specifying whether or not the two points do not have equal X, Y, and Z coordinates.</returns>
    public static bool operator !=(Point3D p1, Point3D p2) {
      return !p1.Equals(p2);
    }

    /// <summary>
    /// Check whether or not a given object is equal to this point.
    /// </summary>
    /// <param name="o">Object to compare.</param>
    /// <returns>Whether or not the object is equal to this point.</returns>
    public override bool Equals(object o) {
      if (o.GetType().Equals(typeof(Point3D))) {
        // Other object is a Coordinate
        // Test if object is equal to this Coordinate
        Point3D p2 = (Point3D) o;

        return (this.X == p2.X) &&
                (this.Y == p2.Y) &&
                (this.Z == p2.Z);

      } else {
        return false;
      }
    }

    /// <summary>
    /// Returns a hash code of the X, Y, and Z coordinates of the point.
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode() {
      return X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode();
    }

    /// <summary>
    /// Returns a string of the point.
    /// </summary>
    /// <returns>A string equivalent to (X, Y, Z).</returns>
    public override string ToString() {
      return String.Format("({0}, {1}, {2})", X, Y, Z);
    }
  }
}
