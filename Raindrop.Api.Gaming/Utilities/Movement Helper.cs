﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;

namespace Ostenvighx.Api.Gaming.Utilities {

    public static class MovementHelper {
        
        /// <summary>
        /// Returns a value that determines which angle a object should turn to face another object.
        /// </summary>
        /// <param name="position">Position of object to turn.</param>
        /// <param name="target">Position of thing object should face.</param>
        /// <returns>Double of angle to reposition thing to face the point.</returns>
        public static double FaceObject(PointF position, PointF target) {
            return (Math.Atan2(position.Y - target.Y, position.X - target.X) * (180 / Math.PI));
        }

        /// <summary>
        /// Returns a Vector2 that determines how far an object should move towards a
        /// point given a constant speed.
        /// </summary>
        /// <param name="position">The current position of the object.</param>
        /// <param name="target">A vector determining the target position of the object.</param>
        /// <param name="speed">A constant speed the object should move in.</param>
        /// <returns>Vector2 giving amount to move towards new position.</returns>
        public static PointF MoveTowards(PointF position, PointF target, float speed) {

            if (Math.Abs(position.X - target.X) >= (int) (speed / 2) && Math.Abs(position.Y - target.Y) >= (int) (speed / 2)) {
                double direction = (float)(Math.Atan2(target.Y - position.Y, target.X - position.X) * 180 / Math.PI);

                PointF move = new PointF(0, 0);

                move.X = (float)Math.Cos(direction * Math.PI / 180) * speed;
                move.Y = (float)Math.Sin(direction * Math.PI / 180) * speed;

                return move;
            }

            return PointF.Empty;
        }
    }
}
