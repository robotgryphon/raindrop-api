﻿using System;
using Raindrop.Api.Irc;
using System.Threading;
using System.Text.RegularExpressions;

namespace Tester {
    public class Launch {
        public static void Main(string[] args) {

            IrcConnection conn = new IrcConnection();
            conn.OnMessageRecieved += HandleMessage;
            conn.Me.nickname = "Suibhne";

            TestConnection(conn);

            Console.ReadLine();
        }

        public static void HandleMessage(IrcConnection conn, IrcMessage msg) {
            Console.WriteLine(msg);
        }

        public static void TestConnection(IrcConnection conn) {
            Console.WriteLine("Creating IRC on Localhost.");


            IrcLocation suibhne = new IrcLocation("#suibhne");
            conn.Connect();

            while (conn.Status != IrcReference.ConnectionStatus.Connected) { }

            conn.JoinChannel(suibhne);

            IrcMessage msgTest = new IrcMessage("#suibhne", conn.Me, "");
            msgTest.type = IrcReference.MessageType.ChannelAction;

            msgTest.message = "is performing test: MESSAGES AND NOTICES";
            conn.SendMessage(msgTest);

            msgTest.type = IrcReference.MessageType.ChannelMessage;
            msgTest.message = "Channel Message Test";
            conn.SendMessage(msgTest);

            msgTest.type = IrcReference.MessageType.ChannelAction;
            msgTest.message = "Channel Action Test";
            conn.SendMessage(msgTest);

            Thread.Sleep(1000);

            msgTest.type = IrcReference.MessageType.ChannelAction;

            // Join and Part test announcement
            msgTest.message = "is performing test: JOINING AND PARTING";
            conn.SendMessage(msgTest);

            // Part test
            conn.PartChannel("#suibhne");

            // Join test (should work)
            conn.JoinChannel(suibhne);

            // Part with reason test
            conn.PartChannel("#suibhne", "Testing part reason.");

            // Rejoin channel for further testing
            conn.JoinChannel(suibhne);

            // SENDRAW test. Trying nickname change.
            msgTest.message = "is performing test: Send Raw";
            conn.SendMessage(msgTest);
            conn.SendRaw("NICK Suibhne-");

            conn.Disconnect();
        }
    }
}

